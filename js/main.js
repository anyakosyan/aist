
$(document).ready(function(){

    ////mobile menu
    $('.menu-mobile').click(function () {
        $('.menu-mobile').toggleClass("active");
        $('.menu-block').toggleClass("active");
        $('body').toggleClass(" overflow-hidden");
        $('.main .top-slider_block .prev, .main .top-slider_block .next').toggleClass('index');
    })

});

$('.featured-courses_slider').slick({
  slidesToShow: 3,
  slidesToScroll: 3,
  infinite: false,
  prevArrow: '<button class="btn d-flex justify-content-center align-items-center prev"><i class="fa fa-arrow-left"></i></button>',
  nextArrow: '<button class="btn d-flex  justify-content-center align-items-center next"><i class="fa fa-arrow-right"></i></button>',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,

            }
        },
         {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,

            }
        }
    ]
});